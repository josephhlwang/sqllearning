# mySQL Notes(Advanced)

This is the notes for: https://www.youtube.com/watch?v=7S_tz1z_5bA&t=18s

These notes cover the more advanced concepts of database design and concurrency.

- [mySQL Notes(Advanced)](#mysql-notes-advanced-)
  - [Triggers](#triggers)
    - [Creating a Trigger](#creating-a-trigger)
    - [Deleting a Trigger](#deleting-a-trigger)
  - [Events](#events)
    - [Creating Events](#creating-events)
    - [Viewing Events](#viewing-events)
    - [Deleting Events](#deleting-events)
    - [Altering Events](#altering-events)
  - [Transactions](#transactions)
    - [Creating Transactions](#creating-transactions)
  - [Concurrency](#concurrency)
    - [Common Concurrency Problems](#common-concurrency-problems)
      - [Lost Updates](#lost-updates)
      - [Dirty Reads](#dirty-reads)
      - [Non-repearting Reads](#non-repearting-reads)
      - [Phantom Reads](#phantom-reads)
    - [Setting Isolation Level](#setting-isolation-level)
  - [Deadlocks](#deadlocks)
  - [Datatypes in SQL](#datatypes-in-sql)
    - [Strings](#strings)
    - [Integer](#integer)
    - [Floats](#floats)
    - [Boolean](#boolean)
    - [Enums](#enums)
    - [DATE/TIME](#date-time)
    - [Blob](#blob)
    - [JSON](#json)
      - [Add/Update and Removing Values](#add-update-and-removing-values)
  - [Database Design](#database-design)
    - [Data Modeling](#data-modeling)
    - [Conceptual Models](#conceptual-models)
    - [Logical Models](#logical-models)
    - [Physical Models](#physical-models)
      - [Primary Keys](#primary-keys)
      - [Foreign Keys](#foreign-keys)
      - [Normalization](#normalization)
        - [First Normal Form](#first-normal-form)
        - [Second Normal Form](#second-normal-form)
        - [Third Normal Form](#third-normal-form)
    - [Forward Engineering a Model](#forward-engineering-a-model)
    - [Synchronizing/Reversing Engineering a Model](#synchronizing-reversing-engineering-a-model)
    - [Creating and Dropping Databases](#creating-and-dropping-databases)
    - [Creating Tables](#creating-tables)
    - [Altering Database](#altering-database)
    - [Creating Relationships](#creating-relationships)
    - [Altering Relationships](#altering-relationships)
    - [Storage Engines](#storage-engines)
  - [Indexing](#indexing)
    - [Creating An Index](#creating-an-index)
    - [Show Indexes](#show-indexes)
    - [String Indexes](#string-indexes)
    - [Deleting Indexes](#deleting-indexes)
    - [When Are Indexes Not Used](#when-are-indexes-not-used)

### Triggers

A trigger is a block of SQL code that automatically gets executed before or after an insert, update or delete statement(data modification). Do:

Triggers are usefull for automatic SQL queries or auditting/logging.

#### Creating a Trigger

To create a trigger do,

Ex.

```sql
DELIMITER $$

-- trigger name arbitrary
CREATE TRIGGER payments_after_insert
    -- INSERT could be UPDATE or DELETE
    AFTER INSERT ON payments
    -- how many rows affected
    FOR EACH ROW
BEGIN
    UPDATE invoices
    -- here a row is inserted so the NEW keyword is used to access the new row
    SET payment_total = payment_total + NEW.amount
    WHERE invoice_id = NEW.invoice_id;
END$$

DELIMITER ;
```

This trigger automatically updates invoice total when a new payment is added.
Note that the `OLD` keyword will be used if the trigger is for `DELETE`.

To see the triggers we have created use, `SHOW TRIGGERS`. You can use regex to filter the results.

Ex. `SHOW TRIGGERS LIKE 'payments%'` which will show all triggers of the payments table.

#### Deleting a Trigger

To drop a trigger, use `DROP TRIGGER IF EXISTS <trigger_name>`. This statement is commonly used before creating a new trigger of the same name.

### Events

Events are useful for database maintenance. Events are used to run SQL blocks on a schedule.

#### Creating Events

To create an event do,

```sql
DELIMITER $$

-- event name arbitrary, name should have an time interval
CREATE EVENT yearly_delete_stale_audit_rows
ON SCHEDULE
    -- put interval here and optionally start/end date
    EVERY 1 YEAR STARTS '2019-01-01' ENDS '2029-01-01'
DO BEGIN
    DELETE FROM payments_audit
    WHERE action_date < NOW() - INTERVAL 1 YEAR;
END$$

DELIMITER ;
```

#### Viewing Events

To view events use, `SHOW EVENTS LIKE 'yearly%'`.

#### Deleting Events

To delete events use, `DROP EVENT IF EXISTS yearly_delete_stale_audit_rows`.

#### Altering Events

To alter an event do,

```sql
DELIMITER $$

ALTER EVENT hourly_delete_stale_audit_rows
ON SCHEDULE
    EVERY 1 HOUR STARTS '2019-01-01' ENDS '2029-01-01'
DO BEGIN
    DELETE FROM payments_audit
    WHERE action_date < NOW() - INTERVAL 1 HOUR;
END$$

DELIMITER ;
```

### Transactions

A transaction is a group of SQL statements that represent a single unit of work. If one operation in the transaction fails, the whole transaction fails and any completed operations are reverted.

Operations either all succeed or fail. Transactions follow the [ACID](https://www.ibm.com/docs/en/cics-ts/5.4?topic=processing-acid-properties-transactions) principle.

#### Creating Transactions

To create a transaction do,

```sql
START TRANSACTION;

INSERT INTO orders (customer_id, order_date, status)
VALUES (1, '2019-01-01', 1);

INSERT INTO order_items
VALUES (LAST_INSERT_ID(), 1, 1, 1);

COMMIT;
```

A transaction has no name. Either all statements are commited successfully or the DB does a rollback which undos everything.

### Concurrency

When more than one user tries to access or modify the same data. When two sessions try to access the same row, a "lock" is put on the row. This mean that the row cannot be modified unless it is unlocked. It is unlocked when the transaction is over.

#### Common Concurrency Problems

##### Lost Updates

The default behaviour of a MYSQL DB uses locks when dealing with transactions. When locking is turned off, updates can be lost when two transaction try to update the same row at the same time both not commiting. The update that commits second is kept.

The solution is to use locking.

##### Dirty Reads

Dirty reads happen when another query reads uncommited changes made by a transaction. If that transaction gets rolled back, then the reading query will have data that never existed.

The solution is to use isolation levels, use `READ COMMITTED` to limit a query to only be able to read fully commited data.

##### Non-repearting Reads

A non-repeatable read happens when a transaction reads row 'A', then another query updates row 'A'. When the same transaction reads row 'A' again, it will get another value.

To keep the reads consistence with-in a transaction, use isolation level `REPEATABLE READ` to create a snapshot 'A' for consistency.

##### Phantom Reads

To execute transactions sequentially, use isolation level `SERIALIZABLE`. This is the highest level of isolation level but is the most inefficient.

![](./assets/images/transaction_isolation_levels.png)

This is the levels of isolations and the problems they prevent.

#### Setting Isolation Level

To set a new isolation level use, `SET <SESSION/GLOBAL> TRANSACTION ISOLATION LEVEL <isolation_lvl>`. Use the optional `SESSION` or `GLOBAL` keyword to change the isolation level for the current session/all sesisons.

### Deadlocks

Deadlocks happen who two transactions have locks on rows the other transaction is trying to update. An error is thrown since both transactions will never finish.

To minimize deadlocks you can, follow the same order of columns when updating tables.

### Datatypes in SQL

#### Strings

Store fixed length strings using `CHAR(<len>)`.

If string is not fixed length, use `VARCHAR(<len>)` which uses 64kb.

For mid size text like html use `MEDIUMTEXT` which can store up to 16mb.

For long size text like textbooks use `LONGTEXT` which can store up to 4gb.

Lastly there is `TINYTEXT` and `TEXT` which stores 255b and 64 kb respectively.

Using `VARCHAR` is most efficient for queries since it can be indexed.

#### Integer

Use `TINYINT` which stores [-128, 127], uses 1b.

Use `UNSIGNED TINYINT` which stores [0, 255], this prevents the possibility of negative values, uses 1b.

Use `SMALLINT` which stores [-32k, 32k], uses 2b.

Use `MEDIUMINT` which stores [-8M, 8M], uses 3b.

Use `INT` which stores [-2B, 2B], uses 4b.

Use `BIGINT` which stores [-9Z, 9Z], uses 8b.

#### Floats

Use `DECIMAL(p,s)` which stores _p_ digits with _s_ after the decimal point. This is usually used to store float values.

`DEC`, `NUMERIC` and `FIXED` are alias' for `DECIMAL`.

Use `FLOAT` and `DOUBLE` to store very big or small float values for calculations, uses 4b and 8b respectively.

#### Boolean

Use `BOOL`/`BOOLEAN` to store booleans. These are alias' for `TINYINT`.

#### Enums

Enums are used to store a set of predefined values. Enums are usually not used since they are costly to maintain, so 'look-up tables" are used instead; these are tables that store a range of defined and related values.

Ex. drop downs in html are stored in look-up tables.

#### DATE/TIME

Use `DATE` to store a date.

Use `TIME` to store a time.

Use `DATETIME` to store a date and time.

Use `YEAR` to store a year.

#### Blob

Use blob types to store large amounts of binary data like images, videos, pdfs, etc.

Use `TINYBLOB` to store 255b.

Use `BLOB` to store 65kb.

Use `MEDIUMBLOB` to store 16mb.

Use `LARGEBLOB` to store 5gb.

Files should be kept out of relational databases since they are meant to store structured relational data. Instead directly using a file system is a more efficient way to store files.

Storing files in the DB can inrease DB size, slow down backups, decrease performance and create the need to write code to store files.

#### JSON

Using the JSON datatype, we can store JSON object as strings in each row.

So if

```json
{
  "dimensions": [1, 2, 3],
  "weight": 10,
  "manufacturer": { "name": "sony" }
}
```

was stored in as a JSON object in a _properties_ row under the _products_ table, we could access the weight value in SQL by calling

```sql
SELECT product_id, JSON_EXTRACT(properties, '$.weight')
FROM products
WHERE product_id = 1;
```

which returns _10_. The _$_ represents the root of the JSON object.

Another notation can also be used:

```sql
SELECT product_id, properties -> '$.weight'
FROM products
WHERE product_id = 1;
```

For JSON arrays use, `properties -> '$.dimensions[0]'`.

To return the string values without quotes, use `->>` instead of `->`.

##### Add/Update and Removing Values

Use the `JSON_SET()` and `JSON_REMOVE` functions to add/update and remove name-value pairs within a JSON object.

### Database Design

#### Data Modeling

Before creating the actual DB, model your data first. The simpliest model is usually the best. The steps are:

1. Analyze Business Requirements

2. Build a Conceptual Model (Identifying entities and relationships within the business)

3. Build a Logical Model (An abstract data model independent of DB technology)

4. Build a Physical Model (A refined model and the implementation of 3.)

#### Conceptual Models

Conceptual indentify the entities and their relationships within a business. You can visually represent these relationships with Entity Relationship(ER) or UML diagrams.

Use [Lucid](https://lucid.app/) as a tool to create diagrams.

These models are good for presentations with non-tech people.

#### Logical Models

The only difference is that Logical Models show data types and relationships.

There are three main relationships between entities:

1. one to one
2. one to many
3. many to many

#### Physical Models

##### Primary Keys

In each table, there must be a column that uniquely identify each row. That column has the attribute of a _primary key_.

A table can also have _composite primary keys_ which are a combination of two columns that uniquely identify each row. A single primary key is usually better as it is more scalable.

A typically PKs are the first column with an integer or string datatype and should not be changed.

##### Foreign Keys

Foreign keys in table B for table A, are primary keys of table A that show the relationship between table A and table B. Table A is the parent and table B is the child.

When created foreign keys, you can optionally specify what happens to the foreign key when the primary key in the parent class is modified.

_on-update_ has `RESTRICT` which rejects the updates, `CASCADE` which passes down the updates and `SET NULL` which sets the foreign key to null.

_on-delete_ has the same options.

##### Normalization

To prevent duplicate data, we follow the 7 normal forms, only the first three are important. To be in a higher normal form, all previous normal forms should be satisfied.

###### First Normal Form

Each cell should have a single value and we cannot have repeated columns.

Ex. Say we want to store movies with its genres. To conform with 1FN, we need to create a movies table, a movie_tags table and tags table. Each row in movie could have multiple movies_tags which each point to a tag.

###### Second Normal Form

Each table should describe one entity and every column of that table should describe that entity.

Ex. Say we want to store movies with the dates it was watched, those dates should be stored in a separete table since movie watching dates doesn't describe the movie.

###### Third Normal Form

Each column of the table should be independent such that one table cannot be derive from another. This prevents the need to reduce the need for restructuring and unneeded operations.

Ex. Say we have columns invoice_total, payment_total and balance, where balance = invoice_total - payment_total. Here balance should be dropped since it is derived from invoice_total and payment_total.

#### Forward Engineering a Model

After you create a model in MYSQLWorkbench you can select forward engineer in the dropdown under database to generate the SQL script to implement the model. Use forward engineer to generate a _new_ database.

Other objects like views can also be generated.

#### Synchronizing/Reversing Engineering a Model

When you made a change to a model and want to apply it to another DB, say on another server, use synchronize model in the drop down under database. You can connect to another database to sync the change.

You can also reverse engineer a database by using reverse engineer in the drop down under database. You can connect to another database to generate the SQL script to create the database.

#### Creating and Dropping Databases

To create or drop databases use

`CREATE DATABASE IF NOT EXISTS <db_name>`

or `DROP DATABASE IF EXISTS <db_name>`

#### Creating Tables

To create a table use,

```sql
CREATE TABLE <table_name>
(
    <column_id1>
    ...
    <column_idn>
)
```

with n columns.

Ex.

```sql
CREATE TABLE customer
(
    customer_id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(50) NOT NULL,
    points INT NOT NULL DEFAULT 0,
    email VARCHAR(255) NOT NULL UNIQUE,
)
```

This creates a customers table with a _customer_id_ PK, not null _first_name_, not null points with default value 0 and not null unique email.

#### Altering Database

To alter a table(add, remove columns), do

```sql
ALTER TABLE customers
    ADD last_name VARCHAR(50) NOT NULL AFTER first_name,
    ADD PRIMARY KEY (email)
    ADD city VARCHAR(50) NOT NULL,
    MODIFY COLUMN first_name VARCHAR(55) default '',
    DROP points;
```

#### Creating Relationships

To create another table that uses the above _customers_ as a foreign key, do

```sql
CREATE TABLE orders
(
    order_id INT PRIMARY KEY AUTO_INCREMENT,
    customer_id INT NOT NULL,
    FOREIGN KEY fk_orders_customers (customer_id)
        REFERENCES customers (customer_id)
        ON UPDATE CASCADE
        ON DELETE NO ACTION
)
```

This creates an order table with a _order_id_ PK, not null _customer_id_ FK that refers to the _customer_id_ PK in the customers table. The FK has CASCASE and NO ACTION for ON UPDATE and ON DELETE.

#### Altering Relationships

To alter a FK(add, remove columns), do

```sql
ALTER TABLE orders
    DROP FOREIGN KEY fk_orders_customers,
    ADD FOREIGN KEY fk_orders_customers (customer_id)
        REFERENCES customers (customer_id)
        ON UPDATE CASCADE
        ON DELETE NO ACTION;
```

#### Storage Engines

Using InnoDB in all cases.

### Indexing

You can _index_ a column to increase the speed of a query. Indexing stores a specific column in order in memory so queries do not have to check rows randomly.

Indexing increases database size, slows down writing new rows so reserve indexing for proformence critical queries.

Indexes usuall stored using binary trees.

#### Creating An Index

To create an index on a column of a table, do

```sql
CREATE INDEX idx_state ON customers (state);
```

Now SQL will automatically use this index during queries.

To create _composite indexes_ on multiple columns of a table, do

```sql
CREATE INDEX idx_state_city ON customers (state, city);
```

In a composite index, the order matters. You should order based on the more frequently used columns first, the more unique column first.

#### Show Indexes

To show indexes in a table, do

```sql
SHOW INDEXES IN customers;
```

SQL automatically creates an index on the PK.

#### String Indexes

Storing full string as an index takes a lot of memory. We use prefix indexing on strings to store as little characters as possible while maximizing the number of unique rows.

To store the first n charactors of

```sql
CREATE INDEX idx_last_name ON customers (last_name(n));
```

#### Deleting Indexes

To drop an index, do

```sql
DROP INDEX idx_state ON customers;
```

#### When Are Indexes Not Used

They are not used during expressions
